# ---------------------------------------------------------------------------------------------------------------------
# Project           : Paper "Five-parameter log-normal distribution and its modification"
# Code author       : Holub Jan
# Date created      : 20231216
# Purpose           : Common formatting for parameter vector theta for LN5 and mLN5 distributions.
# ---------------------------------------------------------------------------------------------------------------------

#' Format parameters of the LN5 and mLN5 distributions as a named vector.
#'
#' @param a,b,mu,sigma,y0 Numeric parameter values.
#' @param theta Numeric vector with parameter values.
#' @returns A named numeric vector of length 5.
#' @examples
#' format_theta(1, 1, 0, 1, 0)
#' format_theta(theta = c(1, 1, 0, 1, 0))
format_theta <- function(a = NULL, b = NULL, mu = NULL, sigma = NULL, y0 = NULL, theta = NULL){
  
  theta_out <- c(a, b, mu, sigma, y0)
  
  if ( length(theta_out) == 5 ) {
    names(theta_out) <- c('a', 'b', 'mu', 'sigma', 'y0')
    return(theta_out)
  }

  if(is.list(theta) | is.vector(theta)) {
    
    # if theta is named then correct the order of parameters
    if(!is.null(names(theta))) {
      theta <- theta[c('a', 'b', 'mu', 'sigma', 'y0')]
    } 
    
    # unlist
    theta_out <- as.numeric(unlist(theta))
    
    # does have correct length?
    if ( length(theta_out) == 5 ){
      # out = 1
      names( theta_out ) <- c('a', 'b', 'mu', 'sigma', 'y0')
      return( theta_out )
    }
  }
  
  stop('Parameters were not specified correctly.')
  
}


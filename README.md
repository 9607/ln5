# LN5

## Description
Supplementary scripts for the paper "Five-parameter log-normal distribution and its modification"

## Authors and acknowledgment
Paper authors: Holub Jan, Ladislav Budík, Marie Budíková, Lenka Přibylová and Ivana Horová
E-mail adress: ladislav.budik@chmi.cz

## Software
R version used: 4.2.2
Required packages EnvStats, MASS, flexsurv, ggplot2, cowplot available on CRAN, twopiece available on GitHub (https://github.com/FJRubio67/twopiece)

## Information about results reproduction:
1) set correct working directory in `morava.R` and `dyje.R`
2) run files `morava.R` and `dyje.R` for Table 1 and Table 2 results (tables with local maximum likelihood estimates and AIC)

Copyright (c) 2023 Authors

# ---------------------------------------------------------------------------------------------------------------------
# Project           : Paper "Five-parameter log-normal distribution and its modification"
# Code author       : Holub Jan
# Date created      : 20231216
# Purpose           : Replicate results for Dyje river in Table 1 and results for AIC comparison of different models
#                     in Table 2.
# ---------------------------------------------------------------------------------------------------------------------

# Load required packages and functions
library(EnvStats)  # elnorm3
library(MASS)      # fitdistr
library(flexsurv)  # dgengamma
library(twopiece)  # dtp3

# Change the working directory
setwd(dir = "D:/LN5-paper/paper-code")

source(file = "src/fn_lnorm3.R", chdir = TRUE)
source(file = "src/fn_lnorm5.R", chdir = TRUE)
source(file = "src/fn_lnorm5m.R", chdir = TRUE)
source(file = "src/fn_genweibull.R", chdir = TRUE)
source(file = "src/fn_twopiece.R", chdir = TRUE)


# Read dataset
observed_values = read.table(file = "data/river_dyje.txt", dec = ",")$V1


# Estimate generalized gamma
init_theta_gengamma = list(mu = 2.0, sigma = 0.5, Q = 0)
estimate_gengamma = fitdistr(
  x = observed_values,
  densfun = dgengamma,
  start = init_theta_gengamma,
  method = "Nelder-Mead"
)


# Estimate generalized Weibull
init_theta_genweibull = list(lambda = -2.0, sigma = 4.0, alpha = 0.3)
estimate_genweibull = fitdistr(
  x = observed_values,
  densfun = dgenweibull,
  start = init_theta_genweibull,
  method = "Nelder-Mead"
)


# Estimate two-piece distribution
init_theta_dtp3 = list(mu = 10.0, par1 = 5.0, par2 = 8.0)
estimate_dtp3 = fitdistr(
  x = observed_values,
  densfun = dtp3_wrapper,
  start = init_theta_dtp3,
  FUN = dcauchy,
  method = "Nelder-Mead",
  param = "tp"
)


# Estimate LN3
init_theta_lnorm3 <- elnorm3(x = observed_values)$parameters
estimate_lnorm3 <- optim_llnorm3(
  theta0 = init_theta_lnorm3,
  x = observed_values,
  method = "Nelder-Mead"
)


# Estimate LN5
init_theta_lnorm5 <- c("a" = 2.4, "b" = 1.2, "y0" = -0.055)
estimate_lnorm5 <- optim_llnorm5_profile(
  theta0_aby0 = init_theta_lnorm5,
  x = observed_values,
  method = "Nelder-Mead"
)


# Estimate mLN5
init_theta_lnorm5m <- c("a" = 2.455, "b" = 1.2, "y0" = -0.05)
estimate_lnorm5m <- optim_llnorm5m_profile(
  theta0_aby0 = init_theta_lnorm5m,
  x = observed_values,
  method = "Nelder-Mead"
)


# Create table with lnorm3, lnorm5, mlnorm5 estimates
results_df <- data.frame(
  rbind(
    c(1, 1, estimate_lnorm3$par),
    estimate_lnorm5$par,
    estimate_lnorm5m$par
  )
)
colnames(results_df) <- c("a", "b", "mu", "sigma2", "y0")
results_df["sigma2"] <- results_df["sigma2"]^2

log_likelihoods <- c(
  llnorm5(x = observed_values, theta = c(1, 1, estimate_lnorm3$par)),
  llnorm5(x = observed_values, theta = estimate_lnorm5$par),
  llnorm5m(x = observed_values, theta = estimate_lnorm5m$par)
)

results_df["log_likelihood"] <- log_likelihoods

results_df


# Create table with AIC

aic_lnorm3 = 2 * 3 - 2 * llnorm5(x = observed_values, theta = c(1, 1, estimate_lnorm3$par))
aic_lnorm5 = 2 * 5 - 2 * llnorm5(x = observed_values, theta = estimate_lnorm5$par)
aic_mlnorm5 = 2 * 5 - 2 * llnorm5m(x = observed_values, theta = estimate_lnorm5m$par)

aic = c(
  AIC(estimate_gengamma),
  AIC(estimate_genweibull),
  AIC(estimate_dtp3),
  aic_lnorm3,
  aic_lnorm5,
  aic_mlnorm5
)

names(aic) = c("gengamma", "genweibull", "dtp3", "lnorm3", "lnorm5", "mlnorm5")
aic
